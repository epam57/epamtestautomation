import java.math.BigDecimal;
import java.math.BigInteger;

public class HelloWorld {
    public static void main(String[] args) {

        int a = -128;
        Integer ao=a;
        Integer bo=a;
        System.out.println("a="+a);
        System.out.println("bo=a: "+(bo==a));
        System.out.println("ao=a: "+(ao==a));
        System.out.println("ao=bo: "+(ao==bo));
        a = -129;
        ao=a;
        bo=a;
        System.out.println("a="+a);
        System.out.println("bo=a: "+(bo==a));
        System.out.println("ao=a: "+(ao==a));
        System.out.println("ao=bo: "+(ao==bo));
        a = 127;
        ao=a;
        bo=a;
        System.out.println("a="+a);
        System.out.println("bo=a: "+(bo==a));
        System.out.println("ao=a: "+(ao==a));
        System.out.println("ao=bo: "+(ao==bo));
        a = 128;
        ao=a;
        bo=a;
        System.out.println("a="+a);
        System.out.println("bo=a: "+(bo==a));
        System.out.println("ao=a: "+(ao==a));
        System.out.println("ao=bo: "+(ao==bo));

        int binaryNumber=0b110101100;
        System.out.println(Integer.toBinaryString(binaryNumber));
        Long longBinary=0b1111000000L;
//        binaryNumber=(Integer)longBinary;
        System.out.println("longBinary: "+longBinary);
        long longValue = 1000L;
        int intValue= (int) longValue;
        long longValue2 = 8791798054912L;
        System.out.println("longValue2: "+Long.toBinaryString(longValue2));
        int intValue2=(int)longValue2;
        System.out.println("intValue2: "+Integer.toBinaryString(intValue2));

        Double ad=0.0, bd=0.0;
        System.out.println("0/0= "+ad/bd);
        System.out.println("1/0= "+1/bd);
        System.out.println("-1/0= "+(-1/bd));
//        binaryNumber=(Integer)longBinary;
//        System.out.println(longBinary);

        Integer nulNumber = (Integer) null;
        System.out.println("nulNumber instanceof Integer: "+(nulNumber instanceof Integer));
        System.out.println("ao instanceof Integer: "+(ao instanceof Integer));
        System.out.println("longBinary instanceof Long: "+(longBinary instanceof Long));

        BigInteger bigInteger1, bigInteger2, bigIntegerResult;

        bigInteger1=BigInteger.valueOf(1000000);
        bigInteger2=BigInteger.valueOf(20000000);
        bigIntegerResult= BigInteger.valueOf(1);
        for (int i = 0; i < 10; i++) {
            bigIntegerResult = bigIntegerResult.add(bigInteger1.multiply(bigInteger2).multiply(bigInteger2));
            System.out.println("Result= "+bigIntegerResult);
        }
        BigDecimal bigDecimal1, bigDecimal2, bigDecimalResult;
        bigDecimal1=BigDecimal.valueOf(43242342);
        bigDecimal2=BigDecimal.valueOf(0);
        bigDecimalResult=bigDecimal1.divide(bigDecimal2);
        System.out.println(bigDecimalResult);
    }
}
